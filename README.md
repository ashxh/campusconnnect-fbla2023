# CampusConnnect-FBLA2023

# About

This is the code for CampusConnnect, a project developed by Aadi Shah and Shashi Salavath to compete in the 2023 Coding & Programming event for the Future Business Leaders of America organization.

# Awards

1st Place, Central Northern Area Regionals
1st Place, Illinois State Leadership Conference
9th Place, National Leadership Conference (Hosted in Atlanta, Georgia)

Contact Aaditya Shah for More Information:
ashah1@imsa.edu
