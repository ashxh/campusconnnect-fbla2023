
// search bar animation
$(document).ready(function () {
    $("#search").focus(function () {
      $(".search-box").addClass("border-searching");
      $(".search-icon").addClass("si-rotate");
    });
    $("#search").blur(function () {
      $(".search-box").removeClass("border-searching");
      $(".search-icon").removeClass("si-rotate");
    });
    $("#search").keyup(function () {
      if ($(this).val().length > 0) {
        $(".go-icon").addClass("go-in");
      } else {
        $(".go-icon").removeClass("go-in");
      }
    });
    $(".go-icon").click(function () {
      $(".search-form").submit();
    });
  });

// search logic
function searchText() {
  let input = document.getElementById('search').value;
  input = input.toLowerCase();
  let entries = document.getElementsByTagName("li");
    
  for (let i = 0; i < entries.length; i++) { 
    let entryText = entries[i].innerHTML.toLowerCase();
    
    if (!entryText.includes(input)) {
      entries[i].style.display = "none";
    } else {
      entries[i].style.display = "block";
    }
  }
}

//stat analysis and plot distribution

function analyzeEntries() {
  let points = [];
  firebase.database().ref('studentEntries').on('value', function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
      var pointEntries = childSnapshot.val().pointTotal;
      points.push(pointEntries);
    });
    
    calculateStatistics(points);
  });
}

function calculateStatistics(points) {
  var mean = calculateMean(points);
  var median = calculateMedian(points);
  var mode = calculateMode(points);
  var standardDeviation = calculateStandardDeviation(points);
  var range = calculateRange(points);
  
  swal("Analytics", "Mean: " + mean + "\nMedian: " + median + "\nMode: " + mode + "\nStandard Deviation: " + standardDeviation + "\nRange: " + range, "info");
}

function calculateMean(points) {
  // Calculate the mean (average) of the points
  var sum = points.reduce(function (a, b) {
    return a + b;
  }, 0);
  var mean = sum / points.length;
  return Math.round(mean);
}

function calculateMedian(points) {
  var sortedPoints = points.sort(function (a, b) {
    return a - b;
  });
  var middleIndex = Math.floor(sortedPoints.length / 2);
  var median;

  if (sortedPoints.length % 2 === 0) {
    median = (sortedPoints[middleIndex - 1] + sortedPoints[middleIndex]) / 2;
  } else {
    median = sortedPoints[middleIndex];
  }

  return median;
}

function calculateMode(points) {
  var modeMap = {};
  var maxCount = 0;
  var mode = [];

  for (var i = 0; i < points.length; i++) {
    var value = points[i];
    modeMap[value] = (modeMap[value] || 0) + 1;

    if (modeMap[value] > maxCount) {
      maxCount = modeMap[value];
      mode = [value];
    } else if (modeMap[value] === maxCount) {
      mode.push(value);
    }
  }  
  return mode.join(", ");
}

function calculateStandardDeviation(points) {
  var mean = calculateMean(points);
  var squaredDifferences = points.map(function (point) {
    return Math.pow(point - mean, 2);
  });
  var variance = calculateMean(squaredDifferences);
  var standardDeviation = Math.sqrt(variance);
  return Math.round((standardDeviation * 100)/ 100);
}

function calculateRange(points) {
  var min = Math.min.apply(null, points);
  var max = Math.max.apply(null, points);
  var range = max - min;
  return range;
}

// notify winners logic
function notifyWinners() {
  swal("Success!","Winners Notified","success")
}



/////////////////////////////
//generate PDF REPORT logic//
/////////////////////////////

function downloadReport(snapshot) {
  firebase.database().ref('studentEntries').on('value', function(snapshot) {
    let allStudents = snapshot.val();
    console.log(JSON.stringify(allStudents));
    pdfReport(allStudents);
  });
}

function pdfReport(allStudents) {
  var tableData = [];
  for (const key in allStudents) {
    if (allStudents.hasOwnProperty(key)) {
      const student = allStudents[key];
      const rowData = [
        { text: student.FullName },
        { text: student.EmailAddress },
        { text: student.eventDate },
        { text: student.otherEventTitle },
        { text: student.pointTotal },
        { text: student.sportEventTitle },
        { text: student.studentGrade },
        { text: student.studentID }
      ];
      tableData.push(rowData);
    }
  }

  var docDefinition = {
    info: {
      title: 'CampusConnect Report',
      author: 'CampusConnect',
      subject: 'CampusConnect Generated PDF Report',
    },
    content: [
      {
        text: 'CampusConnect Involvement Report',
        style: 'header',
        color: '#000000',
        margin: [0, 0, 0, 20], 
      },
      {
        text: 'Illinois Mathematics and Science Academy',
        style: 'subheader',
        color: '#274472',
        decoration: 'underline',
        margin: [0, 0, 0, 10],
      },
      {
        table: {
          widths: ['auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto'],
          body: [
            [
              { text: 'Full Name', style: 'tableHeader' },
              { text: 'Email Address', style: 'tableHeader' },
              { text: 'Event Date', style: 'tableHeader' },
              { text: 'Other Event Title', style: 'tableHeader' },
              { text: 'Point Total', style: 'tableHeader' },
              { text: 'Sport Event Title', style: 'tableHeader' },
              { text: 'Student Grade', style: 'tableHeader' },
              { text: 'Student ID', style: 'tableHeader' },
            ],
            ...tableData,
          ],
        },
      },
    ],
    styles: {
      header: {
        fontSize: 24,
        bold: true,
        alignment: 'center',
        margin: [0, 0, 0, 10],
      },
      subheader: {
        fontSize: 16,
        bold: true,
        alignment: 'center',
      },
      tableHeader: {
        fontSize: 12,
        bold: true,
        fillColor: '#274472',
        color: '#FFFFFF',
        alignment: 'center',
        margin: [0, 5, 0, 5],
      },
    },
  };

  pdfMake.createPdf(docDefinition).open();
  pdfMake.createPdf(docDefinition).download('CampusConnectReport.pdf');
}