// -------------------------------------------------------------------------------------------------------------------
// DATABASE & MAIN BACKEND CODE
// -------------------------------------------------------------------------------------------------------------------


//Firebase Configurations

const firebaseConfig = {
    apiKey: "AIzaSyA8zVnO0fL99J99bRxTqcZbmKvdxdYPWDs",
    authDomain: "fblacap2023.firebaseapp.com",
    databaseURL: "https://fblacap2023-default-rtdb.firebaseio.com",
    projectId: "fblacap2023",
    storageBucket: "fblacap2023.appspot.com",
    messagingSenderId: "928148561689",
    appId: "1:928148561689:web:e39e9b05925f81cf78ef88",
    measurementId: "G-JQYWB5VZ5C",
};

// Init Firebase
firebase.initializeApp(firebaseConfig);

// Database Variables
var entryFormDB = firebase.database().ref("studentEntries");
var validationDB = firebase.database().ref("validationDB");

// Init Form
document
    .getElementById("submitForm").addEventListener('submit', submitForm)

// Function to Submit Form


function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function submitForm(e) {
    e.preventDefault();

    var FullName = getElementVal("FullName");
    var EmailAddress = getElementVal("EmailAddress");
    var studentID = getElementVal("studentID")
    var date = getElementVal("date");
    var grades = getElementVal("grades");
    var points = 0;
    var submittedCode = getElementVal("submittedCode")

    //query for tab selector radio type elements
    var sportingEvent = document.querySelector('input[name="sportEvent"]:checked').value;
    var otherEvent = document.querySelector('input[name="otherEvent"]:checked').value;

    if (sportingEvent != "none") {
        //points+=getRandomInt(50)
        otherEvent = "none"
    }
    if (otherEvent != "none") {
        //points+=getRandomInt(50)
        sportingEvent = "none"
    }

    if (otherEvent != "none" || sportingEvent != "none") {
        points++
    }

    //verify data fetching
    console.log(FullName, EmailAddress, studentID, date, grades, sportingEvent, otherEvent, points);
    saveEntry(FullName, EmailAddress, studentID, date, grades, sportingEvent, otherEvent, points, submittedCode);
    document.getElementById("totalEntryForm").reset();

   
}

//add entry to firebase database
const saveEntry = (FullName, EmailAddress, studentID, eventDate, studentGrade, sportEventTitle, otherEventTitle, pointTotal, submittedCode) => {
    var entryName = FullName + "-" + studentID;
    var currPoints = pointTotal;
    var prevPoints = getPointTotal(entryName);
    currPoints += prevPoints;

    const userData = ({
        FullName: FullName,
        EmailAddress: EmailAddress,
        studentID: studentID,
        eventDate: eventDate,
        studentGrade: studentGrade,
        sportEventTitle: sportEventTitle,
        otherEventTitle: otherEventTitle,
        pointTotal: currPoints
    })

    //let newEntry = entryFormDB.child(entryName)
    //newEntry.set(userData)
    checkCode(studentID, submittedCode, entryName, userData)
};

function checkCode(studentID,code, entryName, userData) {
    var sID = studentID
    var code_ref = firebase.database().ref('validationDB/' + code)

    code_ref.on('value', function (snapshot) {
        if (snapshot.val() != undefined && snapshot.val() != null) {
            var data = snapshot.val()
            if(sID == data.studentID) {
                let newEntry = entryFormDB.child(entryName)
                newEntry.set(userData)
                deleteCode(code)
                swal("Success!", "Event Confirmed", "success");
            }
            else {
                swal("Error", "Invalid Submission", "warning");
            }
        }
        else {
            swal("Error!", "Invalid Submission", "warning");
        }
    })
}

// Returns Value of Element at Set ID
const getElementVal = (id) => {
    return document.getElementById(id).value;
};

// Gets Point Total at given userPath

function getPointTotal(userPath) {

    var sent = 0;
    var user_ref = firebase.database().ref('studentEntries/' + userPath);

    user_ref.on('value', function(snapshot) {
        if(snapshot.val() != undefined && snapshot.val() != null) {
            var data = snapshot.val()
            sent = data.pointTotal;
        }
    })
    return sent;
}

// Updates Report Function with Sophomores

function retrieveSophomores() {

    document.getElementById("sophReport").innerHTML="";

    var returnArr = []

    firebase.database().ref('studentEntries').on('value', function(snapshot) {
        snapshot.forEach (
            function(childSnapshot) {
                var FullName = childSnapshot.val().FullName;
                var EmailAddress = childSnapshot.val().EmailAddress;
                var studentID = childSnapshot.val().studentID;
                var points = childSnapshot.val().pointTotal;
                var gradeLevel = childSnapshot.val().studentGrade;

                if (gradeLevel == "10th") {
                    let entry = FullName + " [#" + studentID + "] " + "has " + points + " points" + "\n";
                    returnArr.push(entry);
                }
            }
        )

        for(let entryy of returnArr) {
            update(entryy,"sophReport")
        }
    });
}

function getStandings() {

    document.getElementById("sophStandings").innerHTML="";
    document.getElementById("juniorStandings").innerHTML="";
    document.getElementById("seniorStandings").innerHTML="";
    var sophArr = []
    var juniorArr = []
    var seniorArr = []

    firebase.database().ref('studentEntries').on('value', function(snapshot) {
        snapshot.forEach (
            function(childSnapshot) {
                var FullName = childSnapshot.val().FullName;
                var points = childSnapshot.val().pointTotal;
                var gradeLevel = childSnapshot.val().studentGrade;

                if (gradeLevel == "10th") {
                    let entry = [FullName , points]
                    //let entry = FullName + " [#" + studentID + "] " + "has " + points + " points" + "\n";
                    sophArr.push(entry);
                }
                else if (gradeLevel == "11th") {
                    let entry = [FullName , points]
                    //let entry = FullName + " [#" + studentID + "] " + "has " + points + " points" + "\n";
                    juniorArr.push(entry);
                }
                else if (gradeLevel == "12th") {
                    let entry = [FullName , points]
                    //let entry = FullName + " [#" + studentID + "] " + "has " + points + " points" + "\n";
                    seniorArr.push(entry);
                }

            }
        )

        sophArr.sort(function(a, b) {
            return a[1] - b[1];
        });


        juniorArr.sort(function(a, b) {
            return a[1] - b[1];
        });


        seniorArr.sort(function(a, b) {
            return a[1] - b[1];
        });

        for(var i = sophArr.length-1; i > sophArr.length-6; i--) {
            update(sophArr[i][0] + " with " + sophArr[i][1] + " points.","sophStandings")
        }

        for(var i = juniorArr.length-1; i > juniorArr.length-6; i--) {
            update(juniorArr[i][0] + " with " + juniorArr[i][1] + " points.", "juniorStandings")
        }

        for(var i = seniorArr.length-1; i > seniorArr.length-6; i--) {
            update(seniorArr[i][0] + " with " + seniorArr[i][1] + " points.", "seniorStandings")
        }
    });
}


// Updates Report Function with Juniors

function retrieveJuniors() {

    document.getElementById("juniorReport").innerHTML="";

    var returnArr = []
    firebase.database().ref('studentEntries').on('value', function(snapshot) {
        snapshot.forEach (
            function(childSnapshot) {
                var FullName = childSnapshot.val().FullName;
                var EmailAddress = childSnapshot.val().EmailAddress;
                var studentID = childSnapshot.val().studentID;
                var points = childSnapshot.val().pointTotal;
                var gradeLevel = childSnapshot.val().studentGrade;

                if (gradeLevel == "11th") {
                    let entry = FullName + " [#" + studentID + "] " + "has " + points + " points" + "\n";
                    returnArr.push(entry);
                }

            }
        )

        for(let entryy of returnArr) {
            update(entryy, "juniorReport")
        }
    });
}

// Updates Report Function with Seniors

function retrieveSeniors() {

    document.getElementById("seniorReport").innerHTML="";

    var returnArr = []
    firebase.database().ref('studentEntries').on('value', function(snapshot) {
        snapshot.forEach (
            function(childSnapshot) {
                var FullName = childSnapshot.val().FullName;
                var studentID = childSnapshot.val().studentID;
                var points = childSnapshot.val().pointTotal;
                var gradeLevel = childSnapshot.val().studentGrade;

                if (gradeLevel == "12th") {
                    let entry = FullName + " [#" + studentID + "] " + "has " + points + " points" + "\n";
                    returnArr.push(entry);
                }

            }
        )

        for(let entryy of returnArr) {
            update(entryy, "seniorReport")
        }
    });
}

// Winner Pages Function

function winners() {
    document.getElementById("sophWinners").innerHTML="";
    document.getElementById("juniorWinners").innerHTML="";
    document.getElementById("seniorWinners").innerHTML="";
    document.getElementById("prizeWinners").innerHTML="";

    var prizeWinner = []
    var sophs = []
    var juniors = []
    var seniors = []

    let winnerRef = firebase.database().ref('studentEntries/')

    winnerRef.on('value', function(snapshot) {
        snapshot.forEach (
            function(childSnapshot) {
                var FullName = childSnapshot.val().FullName;
                var studentID = childSnapshot.val().studentID;
                var points = childSnapshot.val().pointTotal;
                var studentGrade = childSnapshot.val().studentGrade;

                let entry = FullName + " [#" + studentID + "]" + "\n";
                if (points >= 50) {
                    prizeWinner.push(entry);
                }


                switch(studentGrade) {
                    case "10th":
                        sophs.push([entry, points]);
                        break;
                    case "11th":
                        juniors.push([entry, points]);
                        break;
                    case "12th":
                        seniors.push([entry, points])
                        break;
                }

            }
        )

        var sophHighest = []
        var juniorHighest = []
        var seniorHighest = []

        for(let soph of sophs) {
            if(sophHighest.length < 1) {sophHighest.push(soph)}
            else {
                if(sophHighest[0][1] < soph[1]) {
                    sophHighest.pop()
                    sophHighest.push(soph)
                }
            }
        }

        for(let junior of juniors) {
            if(juniorHighest.length < 1) {juniorHighest.push(junior)}
            else {
                if(juniorHighest[0][1] < junior[1]) {
                    juniorHighest.pop()
                    juniorHighest.push(junior)
                }
            }
        }

        for(let senior of seniors) {
            if(seniorHighest.length < 1) {seniorHighest.push(senior)}
            else {
                if(seniorHighest[0][1] < senior[1]) {
                    seniorHighest.pop()
                    seniorHighest.push(senior)
                }
            }
        }

        update("Highest Points: " + String(sophHighest[0][0]), "sophWinners");
        update("Highest Points: " + String(juniorHighest[0][0]), "juniorWinners");
        update("Highest Points: " + String(seniorHighest[0][0]), "seniorWinners");

        var randomSoph = sophs[Math.floor(Math.random()*sophs.length)];
        var randomJunior = juniors[Math.floor(Math.random()*juniors.length)];
        var randomSenior = seniors[Math.floor(Math.random()*seniors.length)];

        while(randomSoph[0] == sophHighest[0][0] || randomSoph[1] < 1) {
            randomSoph = sophs[Math.floor(Math.random() * sophs.length)];
        }

        while(randomJunior[0] == juniorHighest[0][0] || randomJunior[1] < 1) {
            randomJunior = juniors[Math.floor(Math.random()*juniors.length)];
        }

        while(randomSenior[0] == seniorHighest[0][0] || randomSenior[1] < 1) {
            randomSenior = seniors[Math.floor(Math.random()*seniors.length)];
        }

        update("Random Winner: " + String(randomSoph[0]), "sophWinners");
        update("Random Winner: " + String(randomJunior[0]), "juniorWinners");
        update("Random Winner: " + String(randomSenior[0]), "seniorWinners");

        for(let winner of prizeWinner) {
            update(winner, "prizeWinners")
        }
    });
}

// Switches to Report Page

function generateReport() {
    document.location = "report.html";
}

// Switches to Winners Page

function generateWinners() {
    document.location = "winners.html";
}

function leaderboards() {
    document.location = "leaderboards.html"
}

// Updates UL at ulID with given text

function update(text, ulID) {
    // Add New LI's
    var node = document.createElement('li');
    node.appendChild(document.createTextNode(text))
    document.getElementById(ulID).appendChild(node)
}

function getLastCode() {
    var curr = 0

    validationDB.on('value', function(snapshot) {
        snapshot.forEach (
            function(childSnapshot) {
                let num = childSnapshot.val().Number;

                curr=Math.max(curr,num)
            }
        )
    });

    return curr
}

function makeCode() {
    setCodes("Aadi Shah", "125188","ashah1@imsa.edu")
}

function setCodes(FullName, studentID, EmailAddress) {
    //let codeNumber = getLastCode()+1
    let codeNumber = 333;

    const codeData = ({
        Number: codeNumber,
        FullName: FullName,
        EmailAddress: EmailAddress,
        studentID: studentID
    })

    var newCode = validationDB.child(codeNumber)
    newCode.set(codeData)
}

function deleteCode(code) {
    let deadCode = firebase.database().ref('validationDB/' + code)
    deadCode.remove()
}

// Open Firebase

function retrieveDatabase() {
    window.open('https://console.firebase.google.com/project/fblacap2023/database/fblacap2023-default-rtdb/data')
}