// JavaScript File for solely index.html

// Function for TypeSelector
function openTab(evt, tabName) {
  //fetch all elements with class tab-content and hide
  let tabContent = document.getElementsByClassName("tab-content");
  for (let i = 0; i < tabContent.length; i++) {
      tabContent[i].style.display = "none";
  }

  //fetch all elemeknts with class tab-btn and remove class active
  let tabBtns = document.getElementsByClassName("tab-btn");
  for (let i = 0; i < tabBtns.length; i++) {
      tabBtns[i].className = tabBtns[i].className.replace(" active", "");
  }

  //show current tab, and add an active class btn that opened tab
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}

//default tab
document.getElementById("section1").style.display = "block";

//help modal 
const modal = document.querySelector(".modal");
const overlay = document.querySelector(".overlay");
const openModalBtn = document.querySelector(".btn-open");
const closeModalBtn = document.querySelector(".btn-close");

const openModal = function () {
  modal.classList.remove("hidden");
  overlay.classList.remove("hidden");
};
openModalBtn.addEventListener("click", openModal);

const closeModal = function () {
  modal.classList.add("hidden");
  overlay.classList.add("hidden");
};
closeModalBtn.addEventListener("click", closeModal);
overlay.addEventListener("click", closeModal);



// Sign In Modal
const signmodal = document.querySelector(".signmodal");
const signoverlay = document.querySelector(".signoverlay");
const signopenModalBtn = document.querySelector(".btn-open-signin");
const signcloseModalBtn = document.querySelector(".sign-btn-close");

const signopenModal = function () {
  console.log('hello');
  signmodal.classList.remove("hidden");
  signoverlay.classList.remove("hidden");
};
signopenModalBtn.addEventListener("click", signopenModal);

const signcloseModal = function () {
  signmodal.classList.add("hidden");
  signoverlay.classList.add("hidden");
};
signcloseModalBtn.addEventListener("click", signcloseModal);
signoverlay.addEventListener("click", signcloseModal);

// sign in auth
function adminAccess() {
  const username = document.getElementById("AdminUsername").value;
  const password = document.getElementById("AdminPassword").value;
  console.log(username)
  // Check if the entered username and password are correct
  if (username === "admin" && password === "admin") {
    swal("Login Successful!", "Welcome Admin", "success");
    document.location = "admin.html";
  } else {
    swal("Login Failed", "Invalid Credentials", "warning");
  }
}
